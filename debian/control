Source: golang-k8s-kube-openapi
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Andrej Shadura <andrewsh@debian.org>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-golang,
 golang-any,
 golang-github-asaskevich-govalidator-dev,
 golang-github-davecgh-go-spew-dev,
 golang-github-emicklei-go-restful-dev,
 golang-github-getkin-kin-openapi-dev,
 golang-github-go-openapi-jsonreference-dev,
 golang-github-go-openapi-swag-dev,
 golang-github-google-gnostic-models-dev,
 golang-github-google-gofuzz-dev,
 golang-github-google-uuid-dev,
 golang-github-googleapis-gnostic-dev (<< 0.4.1),
 golang-github-json-iterator-go-dev,
 golang-github-kubernetes-gengo-dev (>= 0.0~git20210915.39e73c8-2~),
 golang-github-mitchellh-mapstructure-dev,
 golang-github-munnerz-goautoneg-dev,
 golang-github-nytimes-gziphandler-dev,
 golang-github-onsi-ginkgo-dev,
 golang-github-spf13-pflag-dev,
 golang-github-stretchr-testify-dev,
 golang-gomega-dev,
 golang-goprotobuf-dev,
 golang-k8s-klog-dev,
 golang-k8s-sigs-json-dev,
 golang-k8s-sigs-structured-merge-diff-dev,
 golang-k8s-sigs-yaml-dev,
 golang-k8s-utils-dev (>= 0.0~git20210629),
 golang-yaml.v2-dev,
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-k8s-kube-openapi
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-k8s-kube-openapi.git
Homepage: https://github.com/kubernetes/kube-openapi
Rules-Requires-Root: no
XS-Go-Import-Path: k8s.io/kube-openapi

Package: golang-k8s-kube-openapi-dev
Architecture: all
Multi-Arch: foreign
Depends:
 golang-github-asaskevich-govalidator-dev,
 golang-github-davecgh-go-spew-dev,
 golang-github-emicklei-go-restful-dev,
 golang-github-getkin-kin-openapi-dev,
 golang-github-go-openapi-jsonreference-dev,
 golang-github-go-openapi-swag-dev,
 golang-github-google-gnostic-models-dev,
 golang-github-google-gofuzz-dev,
 golang-github-google-uuid-dev,
 golang-github-googleapis-gnostic-dev (<< 0.4.1),
 golang-github-json-iterator-go-dev,
 golang-github-kubernetes-gengo-dev (>= 0.0~git20210915.39e73c8-2~),
 golang-github-mitchellh-mapstructure-dev,
 golang-github-munnerz-goautoneg-dev,
 golang-github-nytimes-gziphandler-dev,
 golang-github-onsi-ginkgo-dev,
 golang-github-spf13-pflag-dev,
 golang-github-stretchr-testify-dev,
 golang-gomega-dev,
 golang-goprotobuf-dev,
 golang-k8s-klog-dev,
 golang-k8s-sigs-json-dev,
 golang-k8s-sigs-structured-merge-diff-dev,
 golang-k8s-sigs-yaml-dev,
 golang-k8s-utils-dev (>= 0.0~git20210629),
 golang-yaml.v2-dev,
 ${misc:Depends},
Description: Kubernetes OpenAPI spec generation & serving (library)
 Kube OpenAPI This repo is the home for Kubernetes OpenAPI discovery
 spec generation. The goal is to support a subset of OpenAPI features
 to satisfy kubernetes use-cases but implement that subset with little
 to no assumption about the structure of the code or routes. Thus, there
 should be no kubernetes specific code in this repo.
 .
 There are two main parts:
  - A model generator that goes through .go files, find and generate model
 definitions.
  - The spec generator that is responsible for dynamically generate
 the final OpenAPI spec using web service routes or combining
 other OpenAPI/Json specs.  Contributing Please see CONTRIBUTING.md
 (CONTRIBUTING.md) for instructions on how to contribute.

Package: kube-openapi
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Built-Using: ${misc:Built-Using}
Description: Kubernetes OpenAPI spec generation & serving (program)
 Kube OpenAPI This repo is the home for Kubernetes OpenAPI discovery
 spec generation. The goal is to support a subset of OpenAPI features
 to satisfy kubernetes use-cases but implement that subset with little
 to no assumption about the structure of the code or routes. Thus, there
 should be no kubernetes specific code in this repo.
 .
 There are two main parts:
  - A model generator that goes through .go files, find and generate model
 definitions.
  - The spec generator that is responsible for dynamically generate
 the final OpenAPI spec using web service routes or combining
 other OpenAPI/Json specs.  Contributing Please see CONTRIBUTING.md
 (CONTRIBUTING.md) for instructions on how to contribute.
